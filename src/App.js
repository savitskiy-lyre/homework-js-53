import ToDoList from "./components/ToDoList/ToDoList";

const App = () => (
  <div className="App">
     <ToDoList />
  </div>
);

export default App;
